﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class StartMenu : MonoBehaviour {

	public GameObject startObj;
	public Text startText;
	public GameObject LevelViewCamera;
	public GameObject PlayerCamera;
	
	private string m_startString;

	void Start()
	{
		Time.timeScale = 1;
		Screen.showCursor = true;
		m_startString = "Red hurts \nBlue box is goal \nCollect " + ProgresManager.winCondition + " coins to open goal \nGo fast";
		startText.text = m_startString;
		ProgresManager.gameActive = false;
		LevelViewCamera.SetActive (true);
		PlayerCamera.SetActive (false);

	}
	void Update()
	{
		if(Input.GetKeyDown(KeyCode.Return))
		{
			ProgresManager.StartGame();
			startObj.SetActive(false);
			LevelViewCamera.SetActive(false);
			PlayerCamera.SetActive(true);
		}
	}
	public void ReturnMenu()
	{
		Debug.Log ("Retunr menu");
		ProgresManager.deaths = 0;
		Application.LoadLevel (0);
	}
}