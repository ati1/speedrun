﻿using UnityEngine;
using System.Collections;

public class Reset : MonoBehaviour {

	void Update () 
	{
		if(Input.GetKeyDown(KeyCode.Escape))
		{
			ResetLevel();
		}
	}
	public void ResetLevel()
	{
		ProgresManager.deaths += 1;
		Application.LoadLevel(Application.loadedLevel);
	}
}