﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ProgresManager : MonoBehaviour {
	
	public static int deaths = 0;
	public static int winCondition = 0;
	public static bool win;
	public static bool gameActive;
	public int setWinCondition = 0;
	public int amountCollected = 0;

	public Text timeText;
	public Text deathText;
	public Text collectedText;

	private float timer;
	private static TriggerScript[] m_triggers;
	private static MoveObject[] m_moveObjs;

	void Awake()
	{
		m_triggers = FindObjectsOfType<TriggerScript> ();
		m_moveObjs = FindObjectsOfType<MoveObject> ();
		foreach(TriggerScript trigger in m_triggers)
		{
			trigger.enabled = false;
		}
		foreach(MoveObject moveObj in m_moveObjs)
		{
			moveObj.enabled = false;
		}
		win = false;
		winCondition = setWinCondition;
		deathText.text = "Deaths: " + deaths.ToString();
	}

	void Update()
	{
		if(gameActive)
		{
			timer += Time.deltaTime;
			timeText.text = "Time: " + timer.ToString("F2");
			collectedText.text = "Collected: " + amountCollected.ToString () + "/" + winCondition.ToString ();
			
			if(win)
			{
				Time.timeScale = 0;
			}
		}
	}
	public static void StartGame()
	{
		Screen.showCursor = false;
		foreach(TriggerScript trigger in m_triggers)
		{
			trigger.enabled = true;
		}
		foreach(MoveObject moveObj in m_moveObjs)
		{
			moveObj.enabled = true;
		}
		FindObjectOfType<MouseLook> ().enabled = true;
		gameActive = true;
		Time.timeScale = 1;
	}
}