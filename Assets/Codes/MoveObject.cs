﻿using UnityEngine;
using System.Collections;

public class MoveObject : MonoBehaviour {

	public bool callOnAwake;
	public Vector3 dir;
	public float speed;
	public float duration;
	public float waitTime;


	private Transform m_transform;
	private bool moving = false;
	private float timer = 0;
	private float m_duration = 0;

	void Awake()
	{
		m_transform = transform;
		m_duration = duration;

		if(callOnAwake)
		{
			Move();
		}
	}

	void FixedUpdate()
	{
		if(moving)
		{
			if(timer >= m_duration)
			{
				moving = false;
			}
			else
			{
				m_transform.Translate(dir * speed * Time.deltaTime);
			}
			timer += Time.deltaTime;
		}
	}

	public void Move()
	{
		StartCoroutine ("WaitTime");
	}

	IEnumerator WaitTime()
	{
		yield return new WaitForSeconds(waitTime);
		moving = true;
		Move ();
	}
}