﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TriggerScript : MonoBehaviour {

	public Transform teleportLocation;
	public GameObject winText;
	public List<MoveObject> objectsToMove;

	public enum TriggerType
	{
		Damage,
		Collect,
		Multi,
		Teleport,
		Move,
		Goal
	};

	public TriggerType triggerType;


	void OnTriggerEnter(Collider col)
	{
		if (col.GetComponent<CharacterController>())
		{
			switch (triggerType)
			{
			case TriggerType.Damage:
				DamageTrigger(col.gameObject);
				break;
			case TriggerType.Collect:
				CollectTrigger(col.gameObject);
				break;
			case TriggerType.Multi:
				MultiTrigger(col.gameObject);
				break;
			case TriggerType.Teleport:
				Teleport(col.gameObject);
				break;
			case TriggerType.Move:
				MoveTrigger();
				break;
			case TriggerType.Goal:
				Goal(col.gameObject);
				break;
			}
		}
	}
	void DamageTrigger(GameObject player)
	{
		player.GetComponent<Reset>().ResetLevel();
	}
	void CollectTrigger(GameObject player)
	{
		ProgresManager manager = player.GetComponent<ProgresManager> ();
		manager.amountCollected += 1;
		if(manager.amountCollected >= ProgresManager.winCondition)
		{
			TriggerScript[] triggers = FindObjectsOfType<TriggerScript>();
			foreach(TriggerScript trigger in triggers)
			{
				if(trigger.triggerType == TriggerType.Goal)
				{
					trigger.collider.isTrigger = true;
				}
			}
		}
		gameObject.SetActive (false);
	}
	void MultiTrigger(GameObject player)
	{
		//WIP
	}
	void Teleport(GameObject player)
	{
		player.transform.position = teleportLocation.position;
	}
	void MoveTrigger()
	{
		foreach(MoveObject obj in objectsToMove)
		{
			obj.Move();
		}
	}
	void Goal(GameObject player)
	{
		ProgresManager manager = player.GetComponent<ProgresManager> ();
		if(manager.amountCollected >= ProgresManager.winCondition)
		{
			Debug.Log("You're winner!");
			winText.SetActive(true);
			ProgresManager.win = true;
		}
	}
}