﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Menu : MonoBehaviour {

	public GameObject mainMenu;
	public GameObject levelSelect;
	public GameObject options;

	void Awake()
	{
		ShowMainmenu ();
	}

	public void ShowLevels()
	{
		mainMenu.SetActive (false);
		options.SetActive (false);
		levelSelect.SetActive(true);
	}
	public void ShowMainmenu()
	{
		mainMenu.SetActive (true);
		options.SetActive (false);
		levelSelect.SetActive(false);
	}
	public void ShowOptions()
	{
		mainMenu.SetActive (false);
		options.SetActive (true);
		levelSelect.SetActive(false);
	}
	public void ExitGame()
	{
		Application.Quit ();
	}

	public void LoadLevel(string lvlName)
	{
		Application.LoadLevel (lvlName);
	}
}